/**
* @file CKEditor plugin for passing entity info to the Media broswer dialog.
*/

(function($) {
  // Register the plugin.
  CKEDITOR.plugins.add('media_warehouse_entity_info',
  {
    init : function(editor)
    {
      // When the Media button is clicked.
      editor.on('beforeCommandExec', function(evt) {
        if (evt.data.name == 'media') {
          // Gather the Entity Type and Entity ID from the hidden form fields.
          var mw_nid = $("form input[name=mw_entity_type]").val() || '';
          var mw_mmtid = $("form input[name=mw_entity_id]").val() || 0;

          // Store the Entity Type and ID in the Media CKEditor plugin so we
          // have access to them inside the Media browser dialog.
          if (mw_nid.length > 0) {
            Drupal.settings.ckeditor.plugins['media'].global = $.extend(Drupal.settings.ckeditor.plugins['media'].global, {'mw_entity_type' : mw_nid});
          }
          if (mw_mmtid.length > 0) {
            Drupal.settings.ckeditor.plugins['media'].global = $.extend(Drupal.settings.ckeditor.plugins['media'].global, {'mw_entity_id' : mw_mmtid});
          }
        }
      });
    }
  });
})(jQuery);