<?php
/**
 * @file
 * The default view for the Media Warehouse browser tab.
 */

$view = new view();
$view->name = 'media_warehouse_browser';
$view->description = 'Default view for the Media Warehouse Browser tab.';
$view->tag = 'media, default';
$view->base_table = 'file_managed';
$view->human_name = 'Media Warehouse Browser';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Media Warehouse Browser';
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'view files';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['distinct'] = TRUE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['submit_button'] = 'Search';
$handler->display->display_options['exposed_form']['options']['autosubmit_hide'] = FALSE;
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'media_browser';
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = 'No files available.';
$handler->display->display_options['empty']['area']['format'] = 'filtered_html';
/* Relationship: File: Media Warehouse File Tag (field_media_warehouse_tags) */
$handler->display->display_options['relationships']['field_media_warehouse_tags_tid']['id'] = 'field_media_warehouse_tags_tid';
$handler->display->display_options['relationships']['field_media_warehouse_tags_tid']['table'] = 'field_data_field_media_warehouse_tags';
$handler->display->display_options['relationships']['field_media_warehouse_tags_tid']['field'] = 'field_media_warehouse_tags_tid';
/* Field: File: Rendered */
$handler->display->display_options['fields']['rendered']['id'] = 'rendered';
$handler->display->display_options['fields']['rendered']['table'] = 'file_managed';
$handler->display->display_options['fields']['rendered']['field'] = 'rendered';
$handler->display->display_options['fields']['rendered']['label'] = '';
$handler->display->display_options['fields']['rendered']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['rendered']['file_view_mode'] = 'preview';
/* Field: File: Name */
$handler->display->display_options['fields']['filename']['id'] = 'filename';
$handler->display->display_options['fields']['filename']['table'] = 'file_managed';
$handler->display->display_options['fields']['filename']['field'] = 'filename';
$handler->display->display_options['fields']['filename']['label'] = '';
$handler->display->display_options['fields']['filename']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['filename']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['filename']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['filename']['link_to_file'] = FALSE;
/* Sort criterion: File: Upload date */
$handler->display->display_options['sorts']['timestamp']['id'] = 'timestamp';
$handler->display->display_options['sorts']['timestamp']['table'] = 'file_managed';
$handler->display->display_options['sorts']['timestamp']['field'] = 'timestamp';
$handler->display->display_options['sorts']['timestamp']['order'] = 'DESC';
$handler->display->display_options['sorts']['timestamp']['exposed'] = TRUE;
$handler->display->display_options['sorts']['timestamp']['expose']['label'] = 'Upload date';
/* Contextual filter: Global: Null */
$handler->display->display_options['arguments']['null']['id'] = 'null';
$handler->display->display_options['arguments']['null']['table'] = 'views';
$handler->display->display_options['arguments']['null']['field'] = 'null';
$handler->display->display_options['arguments']['null']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['null']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['null']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['null']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['filter_groups']['groups'] = array(
  1 => 'AND',
  2 => 'OR',
);
/* Filter criterion: File: Status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'file_managed';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = array(
  1 => '1',
);
$handler->display->display_options['filters']['status']['group'] = 1;
/* Filter criterion: File: Path */
$handler->display->display_options['filters']['uri']['id'] = 'uri';
$handler->display->display_options['filters']['uri']['table'] = 'file_managed';
$handler->display->display_options['filters']['uri']['field'] = 'uri';
$handler->display->display_options['filters']['uri']['operator'] = 'starts';
$handler->display->display_options['filters']['uri']['value'] = 'mw://';
$handler->display->display_options['filters']['uri']['group'] = 1;
/* Filter criterion: File: Name */
$handler->display->display_options['filters']['filename']['id'] = 'filename';
$handler->display->display_options['filters']['filename']['table'] = 'file_managed';
$handler->display->display_options['filters']['filename']['field'] = 'filename';
$handler->display->display_options['filters']['filename']['operator'] = 'word';
$handler->display->display_options['filters']['filename']['group'] = 2;
$handler->display->display_options['filters']['filename']['exposed'] = TRUE;
$handler->display->display_options['filters']['filename']['expose']['operator_id'] = 'filename_op';
$handler->display->display_options['filters']['filename']['expose']['label'] = 'Name';
$handler->display->display_options['filters']['filename']['expose']['operator'] = 'filename_op';
$handler->display->display_options['filters']['filename']['expose']['identifier'] = 'filename';
/* Filter criterion: Media Warehouse: File Key */
$handler->display->display_options['filters']['mwkey']['id'] = 'mwkey';
$handler->display->display_options['filters']['mwkey']['table'] = 'media_warehouse_files';
$handler->display->display_options['filters']['mwkey']['field'] = 'mwkey';
$handler->display->display_options['filters']['mwkey']['operator'] = 'word';
$handler->display->display_options['filters']['mwkey']['value'] = '***!filename***';
$handler->display->display_options['filters']['mwkey']['group'] = 2;
/* Filter criterion: Taxonomy term: Name */
$handler->display->display_options['filters']['name']['id'] = 'name';
$handler->display->display_options['filters']['name']['table'] = 'taxonomy_term_data';
$handler->display->display_options['filters']['name']['field'] = 'name';
$handler->display->display_options['filters']['name']['relationship'] = 'field_media_warehouse_tags_tid';
$handler->display->display_options['filters']['name']['operator'] = 'word';
$handler->display->display_options['filters']['name']['value'] = '***!filename***';
$handler->display->display_options['filters']['name']['group'] = 2;
