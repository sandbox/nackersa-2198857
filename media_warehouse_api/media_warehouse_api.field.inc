<?php
/**
 * @file
 * Field module functionality for the Media Warehouse API module.
 */

/**
 * Implements hook_field_info().
 */
function media_warehouse_api_field_info() {
  return array(
    'media_warehouse_perms' => array(
      'label' => t('Media Warehouse Permissions'),
      'description' => t('This field stores a files read/write permissions.'),
      'settings' => array('max_length' => ''),
      'default_widget' => 'media_warehouse_perms_selector',
      'default_formatter' => 'media_warehouse_perms_format',
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 */
function media_warehouse_api_field_widget_info() {
  return array(
    'media_warehouse_perms_selector' => array(
      'label' => t('Permission Selector'),
      'field types' => array('media_warehouse_perms'),
    ),
  );
}

/**
 * Implements hook_field_is_empty().
 */
function media_warehouse_api_field_is_empty($item, $field) {
  // Declare how field is empty.
  if (empty($item)) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Implements hook_theme().
 */
function media_warehouse_api_theme() {
  return array(
    'media_warehouse_perms_table' => array(
      'render element' => 'element'
    )
  );
}

/**
 * Implements hook_theme().
 */
function theme_media_warehouse_perms_table($vars) {
  // Default table theme for Media Warehouse permission selector.
  $element = $vars['element'];
  // Define the table headers.
  $header = array(
    'title' => t('Permission'),
    'read' => array('data' => t('Read')),
    'delete' => array('data' => t('Delete')),
    'update' => array('data' => t('Update')),
  );
  // Each row contains the permission name as well as read, delete, and update
  // permission options.
  $rows = array();
  foreach (element_get_visible_children($element) as $key) {
    $rows[] = array(
      array('data' => render($element[$key]['title'])),
      array('data' => render($element[$key]['read'])),
      array('data' => render($element[$key]['delete'])),
      array('data' => render($element[$key]['update'])),
    );
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Implements hook_field_widget_form().
 */
function media_warehouse_api_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $scheme = '';
  if (!empty($element['#entity']) && !empty($element['#entity']->uri)) {
    $scheme = file_uri_scheme($element['#entity']->uri);
  }
  if ($scheme == 'mw' && $instance['widget']['type'] == 'media_warehouse_perms_selector') {
    // Gather all information about the available permission sets.
    $permission_sets = module_invoke_all('media_warehouse_api_perms_info');
    drupal_alter('media_warehouse_api_perms_info', $permission_sets);
    // Create a permissions fieldset.
    if (!empty($permission_sets)) {
      $element += array(
        '#type' => 'fieldset',
      );

      foreach ($permission_sets as $perm_key => $perm_info) {
        if (!empty($perm_info['permission_set']) && is_callable($perm_info['permission_set'])) {
          $applied_perms = array(
            'read' => array(),
            'delete' => array(),
            'update' => array(),
          );
          // Get a listing of permissions.
          $permissions = call_user_func($perm_info['permission_set']);
          // If the file has permissions set, load those permissions.
          if (!empty($items) && !empty($items[0])) {
            $applied_perms['read'] = _media_warehouse_api_get_perms('read', $items[0], $perm_key);
            $applied_perms['delete'] = _media_warehouse_api_get_perms('delete', $items[0], $perm_key);
            $applied_perms['update'] = _media_warehouse_api_get_perms('update', $items[0], $perm_key);
          }
          // Label for the permission.
          $element[$perm_key] = array(
            '#type' => 'fieldset',
            '#title' => t($perm_info['label']),
            '#collapsible' => TRUE,
            '#collapsed' => TRUE,
            '#theme' => isset($perm_info['theme'])? $perm_info['theme']: 'media_warehouse_perms_table',
          );
          // Items to pass media_warehouse_api_widget_perm.
          $perm_context = array(
            'form' => $form,
            'form_state' => $form_state,
            'element' => $element,
            'perm_key' => $perm_key,
          );
          // Allow modules to alter the permission sets before they are built.
          drupal_alter('media_warehouse_api_widget_perm', $permissions, $applied_perms, $perm_context);
          // Create read/update/delete checkboxes for each permission.
          foreach ($permissions as $id) {
            $form_fields = array();
            $title = $id;
            // Allow modules to provide a "pretty" title rather than a key or
            // index.
            drupal_alter('media_warehouse_api_perm_title', $title, $perm_key);

            $form_fields['title'] = array(
              '#markup' => $title,
            );

            $form_fields['read'] = array(
              '#type' => 'checkbox',
              '#title' => t('Read'),
              '#title_display' => 'invisible',
              '#return_value' => $id,
              '#attributes' => array('class' => array('mw-read-perm')),
              '#default_value' => isset($applied_perms['read'][$id]),
            );

            $form_fields['update'] = array(
              '#type' => 'checkbox',
              '#title' => t('Update'),
              '#title_display' => 'invisible',
              '#return_value' => $id,
              '#attributes' => array('class' => array('mw-update-perm')),
              '#default_value' => isset($applied_perms['update'][$id]),
            );

            $form_fields['delete'] = array(
              '#type' => 'checkbox',
              '#title' => t('Delete'),
              '#title_display' => 'invisible',
              '#return_value' => $id,
              '#attributes' => array('class' => array('mw-delete-perm')),
              '#default_value' => isset($applied_perms['delete'][$id]),
            );

            $element[$perm_key][] = $form_fields;
          }
          $form_context = array(
            'element' => &$element,
            'perm_key' => $perm_key,
          );
          // Allow modules to alter the form on a per permission set basis.
          drupal_alter('media_warehouse_api_widget_form', $form, $form_state, $form_context);
        }
      }
    }
  }

  return $element;
}

/**
 * Implements hook_field_attach_submit().
 */
function media_warehouse_api_field_attach_submit($entity_type, $entity, $form, &$form_state) {
  // We only care about file entities and Media Warehouse permission sets.
  if ($entity_type == 'file'  && !empty($form_state['values']['field_media_warehouse_perms'])) {
    $mw_perms = $form_state['values']['field_media_warehouse_perms'];
    $lang = field_language($entity_type, $entity, 'field_media_warehouse_perms', NULL);
    // Make sure we have a language set and permissions to act on.
    if ($lang !== FALSE && !empty($mw_perms[$lang])) {
      $permission_groups = $mw_perms[$lang][0];
      $read_perms = array();
      $delete_perms = array();
      $update_perms = array();
      // Create arrays for $permission_type['groupname'].
      foreach ($permission_groups as $group_name => $group_perms) {
        if (!empty($group_perms)) {
          foreach ($group_perms as $perm) {
            if (!empty($perm['read'])) {
              $read_perms[$group_name][] = $perm['read'];
            }
            if (!empty($perm['delete'])) {
              $delete_perms[$group_name][] = $perm['delete'];
            }
            if (!empty($perm['update'])) {
              $update_perms[$group_name][] = $perm['update'];
            }
          }
        }
      }
      // Serialize permissions and store and them to the entity object.
      $entity->field_media_warehouse_perms[$lang][0] = array(
        'read' => serialize($read_perms),
        'delete' => serialize($delete_perms),
        'update' => serialize($update_perms),
      );
    }
  }
}

/**
 * Implements hook_field_formatter_info().
 */
function media_warehouse_api_field_formatter_info() {
  return array(
    'media_warehouse_perms_format' => array(
      'label' => t('File Permissions'),
      'field types' => array('media_warehouse_perms'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function media_warehouse_api_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $scheme = '';

  if (!empty($entity) && !empty($entity->uri)) {
    $scheme = file_uri_scheme($entity->uri);
  } 

  if ($scheme == 'mw' && $display['type'] == 'media_warehouse_perms_format') {
    // Gather all information about the available permission sets.
    $permission_sets = module_invoke_all('media_warehouse_api_perms_info');
    if (!empty($permission_sets)) {
      foreach ($permission_sets as $perm_key => $perm_info) {
        $permissions = array();
        // Table headers.
        $headers = array(
          t($perm_info['label']),
          t('Read'),
          t('Update'),
          t('Delete'),
        );
        if (!empty($items) && !empty($items[0])) {
          // Gather the permissions for the requested file.
          $read_perms = _media_warehouse_api_get_perms('read', $items[0], $perm_key);
          $delete_perms = _media_warehouse_api_get_perms('delete', $items[0], $perm_key);
          $update_perms = _media_warehouse_api_get_perms('update', $items[0], $perm_key);
          $permissions = $read_perms + $delete_perms + $update_perms;
        }
        $rows = array();
        // Display each permission group and what permissions that group has
        // on the file.
        if (!empty($permissions)) {
          foreach ($permissions as $rid => $foo) {
            $title = $rid;
            // Allow modules to provide a "pretty" title rather than a key or
            // index
            drupal_alter('media_warehouse_api_perm_title', $title, $perm_key);
            $rows[] = array(
              array('data' => $title),
              array('data' => isset($read_perms[$rid])? t('Yes') : t('No')),
              array('data' => isset($update_perms[$rid])? t('Yes') : t('No')),
              array('data' => isset($delete_perms[$rid])? t('Yes') : t('No')),
            );
          }
        }

        $element[0][] =  array(
          '#theme' => 'table',
          '#header' => $headers,
          '#rows' => $rows,
        );
      }
    }
  }

  return $element;
}

/**
 * Get permissions for a given operation.
 *
 * @param string $op
 *   The operation being performed.
 *   - read
 *   - delete
 *   - update
 *
 * @param string $permissions
 *   Serialized array of permissions
 *
 * @param string $permission_group
 *   The permission group being requested.
 *
 * @return array
 *   An array of permissions based on $op and $permission_group.
 */
function _media_warehouse_api_get_perms($op, $permissions, $permission_group) {
  $perms = array();
  $all_perms = array();
  // Check if there are permissions set for the given operation.
  if (!empty($permissions[$op])) {
    // Unserialize the permission set.
    $all_perms = unserialize($permissions[$op]);
    if (!empty($all_perms[$permission_group])) {
      // Store the permission IDs as the array's keys.
      $perms = array_flip($all_perms[$permission_group]);
    }
  }
  return $perms;
}
