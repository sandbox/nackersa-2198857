<?php
/**
 * @file
 * Hooks provided by the Media Warehouse module.
 */

/**
 * Register a new permission set.
 *
 * @return array
 *   An array keyed with the name of the permission set containing an array 
 *   with various settings for that set.
 *   - label: The readable name of the permission set.
 *   - permission_set: The function to call to gather the available permissions.
 */
function hook_media_warehouse_api_perms_info() {
  return array(
    'media_warehouse_role_perms' => array(
      'label' => 'Role',
      'permission_set' => '_media_warehouse_roles_permission_set',
    ),
  );
} 

/**
 * Maps permission keys to readable permission names.
 *
 * @param string $title
 *   The current title for the permission.
 *
 * @param string $perm_key
 *   The key for the current permission set.
 */
function hook_media_warehouse_api_perm_title_alter(&$title, &$perm_key) {

  if ($perm_key == 'media_warehouse_role_perms') {
    // Load the role based on the role's ID.
    $role = user_role_load($title);
    // Store the role's name.
    $title = $role->name;
  }
}

/**
 * Maps permission keys to readable permission names.
 *
 * @param string $op
 *   The operation being performed.
 *
 * @param object $user
 *   The current user.
 *
 * @param object $file
 *   The file object being requested.
 */
function hook_media_warehouse_user_access($op, $user, $file) {
}

/**
 * Allow modules to alter the form on a per permission set basis.
 *
 * @param array $form
 *   The form structure to which widgets are being attached. This may be a full
 *   form structure, or a sub-element of a larger form.
 *
 * @param array $form_state
 *   An associative array containing the current state of the form.
 *
 * @param array $context
 *   An associative array containing the following key-value pairs:
 *   - element: A form element array containing basic properties for the widget.
 *   - perm_key: The key for the current permission set.
 */
function hook_media_warehouse_api_widget_form_alter(&$form, &$form_state, &$context) {
}

/**
 * Allow modules to alter permission sets before the widget is built.
 *
 * @param array $permissions
 *   All available permissions.
 *
 * @param array $applied_perms
 *   The permissions currently being applied.
 *
 * @param array $context
 *   An associative array containing the following key-value pairs:
 *   - form: A form element array containing basic properties for the widget.
 *   - form_state: An associative array containing the current state of the 
 *     form.
 *   - element: A form element array containing basic properties for the widget.
 *   - perm_key: The key for the current permission set.
 */
function hook_media_warehouse_api_widget_perm_alter(&$permissions, &$applied_perms, $context) {
}
