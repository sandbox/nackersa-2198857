CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------
Media Warehouse is a media management framework that looks to solve various
common file handling issues such as file de-duplication, granular permissions,
and in-place editing of files.

REQUIREMENTS
------------
The following modules are required by Media Warehouse:
- Media (at least 7.x-2.0-alpha3)
- File Entity (at least 7.x-2.0-alpha3)
- Views Argument Substitutions

INSTALLATION
------------
1. Copy the entire Media Warehouse directory to the sites/all/modules directory.
2. Login as an administrator. Enable the Media Warehouse module in the
   Administer -> Modules.
3. Optionally enable the Media Warehouse API module and the Media Warehouse
   Roles module.

CONFIGURATION
-------------
1. Navigate to admin/config/media/media_warehouse and set the Media Warehouse
   file system path.
2. If you are using CKeditor navigate to admin/config/content/ckeditor and
   enable the "Pass along the Entity Type and Entity ID to the Media Browser
   dialog" plugin.
3. It his highly suggested that you make the Media Warehouse streamwrapper your
   default download type in admin/config/media/file-system.

MAINTAINERS
-----------
  * Drew Nackers (nackersa) - https://drupal.org/user/1558486