<?php
/**
 * @file
 * Handles generation of unique file keys.
 */

/**
 * Generates a salt.
 *
 * @return string
 *   A randomly generated salt.
 */
function _media_warehouse_generate_salt() {

  $php_win = PHP_SHLIB_SUFFIX != 'so';
  // Get PHP_VERSION_ID, adapted from
  // http://php.net/manual/en/function.phpversion.php:
  if (!defined('PHP_VERSION_ID')) {
    $php_ver = explode('.', PHP_VERSION);
    $php_ver = ($version[0] * 10000 + $version[1] * 100 + $version[2]);
  }
  else {
    $php_ver = PHP_VERSION_ID;
  }
  // Ideally mcrypt will be available, if not we have a less than ideal fall
  // back.
  if (function_exists('mcrypt_create_iv')) {
    $entropy = mcrypt_create_iv(64, (($php_win && $php_ver < 50300) ? MCRYPT_RAND : MCRYPT_DEV_URANDOM));
  }
  else {
    // Using microtime(TRUE) to return a float value includes the UNIX epoch,
    // which due to limited floating point register space rounds off a bunch of
    // precision from the end of the microtime (really making it just 10x more
    // precise than the millisecond, which isn't very good for entropy); so,
    // create a quick and dirty function here:
    $microtime = create_function('', 'return floatval(microtime());');

    // Older versions of PHP needs to seed the random generator via srand(),
    // but in general it's just a good idea to make sure this is done so we'll
    // just do it without checking PHP versions:
    srand((int) (time() * (0.1 + $microtime())));

    $mt_rand = 'rand';
    $mt_srand = 'srand';
    if (function_exists('mt_rand')) {
      mt_srand((int) ((0.1 + $microtime()) * rand()));
      // Use the Mersenne Twister generator to re-seed the rand() generator:
      srand((int) ((0.1 + $microtime()) * mt_rand()));
      $mt_rand = 'mt_rand';
      $mt_srand = 'mt_srand';
    }
    $mt_srand($microtime()*100000 + memory_get_usage(TRUE));
    $entropy = hash('sha256', uniqid($mt_rand(), TRUE), TRUE) . hash('sha256', uniqid($mt_rand(), TRUE), TRUE);
  }
  // If possible let's use the old salt to get some more entropy.
  $entropy .= base64_decode(variable_get('media_warehouse_salt', ''));

  return base64_encode(hash('sha256', $entropy, TRUE));
}

/**
 * Randomly generates a key to be used as a unique filename.
 *
 * @return string
 *   A randomly generated key.
 */
function _media_warehouse_generate_key() {
  try {
    $lastkey = db_query('SELECT `mwkey` FROM `media_warehouse_files` ORDER BY `fid` DESC LIMIT 1')->fetchField(0);
  }
  catch (Exception $e) {
    $lastkey = '';
  }
  // Salt with our namespace and some randomly generated characters.
  $salt = variable_get('media_warehouse_salt', _media_warehouse_generate_salt());
  // All GO links generated will be of at least this length.
  $min_length = 10;
  // After our algorithm gets to this length, we'll restart with a new seed and
  // try again, starting at min_length.  This upper bound number automatically
  // grows slowly if we keep failing to find a key.
  $max_length = $min_length;
  // Invalid characters (things we don't want in any key).
  $bad_chars = array_flip(str_split('aeiouy', 1));
  // Use SHA-256 as a PRG to generate a random bitstream.
  $seed = $salt . $lastkey;
  $stream = array();
  $alphabet_prg = function(&$seed, &$stream) {
    $seed = hash('sha256', $seed, TRUE);
    for ($i = strlen($seed) - 1; $i >= 0; --$i) {
      $byte = ord($seed[$i]);
      // Each byte has 8 useful bits; we'll be wasteful and map the whole thing
      // down to the 0-25 characters we need (< 5 bits).
      $chr = chr(round(($byte / 255.0) * 25.0) + 97);
      $stream[] = $chr;
    };
  };
  $key_attempt = '';
  $guid_len = 0;
  $last_chr = '';
  $attempts = 0;
  $testable = FALSE;
  // Generate a key.
  while ($guid_len < $min_length || !$testable || !_media_warehouse_insert_key($key_attempt)) {
    // In case this loop iteration does not actually change key_attempt
    // (due to a repeated or "bad" character coming out of the PRG stream),
    // don't waste DB connections trying to insert the same key_attempt value
    // which we've already tried.
    $testable = FALSE;
    // If it seems we somehow exhausted all of the search space, gradually grow
    // the max_length so that, eventually, we'll absolutely find a new key.
    if (++$attempts > 1000) {
      $attempts = 0;
      ++$max_length;
    }
    // Make sure we have stream bytes to work with.
    if (empty($stream)) {
      $alphabet_prg($seed, $stream);
    }
    // Restart the key building if we've hit our max_length.
    if ($guid_len == $max_length) {
      $key_attempt = '';
      $guid_len = 0;
      $last_chr = '';
    }
    // Build the key by grabbing from our byte stream.
    $chr = array_pop($stream);
    if (!isset($bad_chars[$chr]) && $last_chr !== $chr) {
      ++$guid_len;
      $key_attempt .= $chr;
      $last_chr = $chr;
      // We've meaningfully changed key_attempt, so mark it for a test
      // insertion when the loop restarts.
      $testable = TRUE;
    }
  }
  // The key was successfully inserted (with speculative values), so return
  // it as a token of our success.  The calling script will want to db_update()
  // and set the correct URL and set status = 1.
  return $key_attempt;
}

/**
 * Tests to see if the generated key is already in use.
 *
 * @param array $key_attempt
 *   The key to check uniqueness of.
 *
 * @return bool | string
 *   Return FALSE if the key is already in use or the actual key if it is not.
 */
function _media_warehouse_insert_key($key_attempt) {

  $fields = array(
    'mwkey' => $key_attempt,
    'fid' => NULL,
    'uid' => NULL,
  );

  // Try to insert the record.
  try {
    db_insert('media_warehouse_files')
      ->fields($fields)
      ->execute();
  }
  catch (Exception $e) {
    // If insertion failed, just return FALSE (basically, indicate that this
    // key is already taken).
    return FALSE;
  }
  // If the insertion succeeded, we've effectively reserved this key for
  // use by the calling script.
  return $key_attempt;
}