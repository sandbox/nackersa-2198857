<?php
/**
 * @file
 * Media Warehouse admin interface files.
 */

/**
 * Form constructor for the Media Warehouse configuration form.
 *
 * @see _media_warehouse_admin_form_submit()
 *
 * @ingroup forms
 */
function _media_warehouse_admin_form($form, &$form_state) {
  $dir_path = variable_get('media_warehouse_dir_path', NULL);
  if (empty($dir_path)) {
    watchdog('mw', 'Invalid default directory path for Media Warehouse.', array(), WATCHDOG_ERROR, NULL);
  }

  $form['file_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Media Warehouse file system path'),
    '#default_value' => $dir_path,
    '#required' => TRUE,
  );
  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Submit',
  );
  return $form;
}
/**
 * Form submission handler for _media_warehouse_admin_form().
 *
 */
function _media_warehouse_admin_form_submit($form, &$form_state) {
  // @todo: Put text stating that if you change directory paths after you have
  // uploaded any amount of files, Media Warehouse may see new files as
  // duplicates.  To fix this issues, all files updated to the previous
  // location should be removed in Drupal.
  if (!empty($form_state['values']['file_path'])) {
    $dir = $form_state['values']['file_path'];
    if (is_dir($dir) && is_writable($dir)) {
      file_create_htaccess($dir, TRUE, FALSE);
      variable_set('media_warehouse_dir_path', $dir);
    }
    else {
      // @todo: This error needs to be more descriptive.
      drupal_set_message(t('Invalid path.'), 'error');
    }
  }
}

/**
 * Form constructor for the Media Warehouse salt regeneration form.
 *
 * @see _media_warehouse_admin_key_form_submit()
 *
 * @ingroup forms
 */
function _media_warehouse_admin_key_form($form, &$form_state) {
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Generate New Salt'),
  );
  return $form;
}

/**
 * Form submission handler for _media_warehouse_admin_key_form().
 *
 */
function _media_warehouse_admin_key_form_submit($form, &$form_state) {
  module_load_include('inc', 'media_warehouse', 'media_warehouse_key');
  variable_set('media_warehouse_salt', _media_warehouse_generate_salt());
}