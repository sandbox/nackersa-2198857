<?php
/**
 * @file
 * Media Warehouse stream wrapper.
 */

class MediaWarehouseStreamWrapper implements DrupalStreamWrapperInterface {

  /**
   * A generic resource handle.
   *
   * @var Resource
   */
  public $handle = NULL;

  /**
   * A readir query handle.
   *
   * @var Resource
   */
  public $dir_query = NULL;

  /**
   * Instance URI (stream).
   *
   * A stream is referenced as "mw://target".
   *
   * @var String
   */
  protected $uri;

  /**
   * Current directory.
   *
   *
   * @var String
   */
  protected $dir;

  /**
   * Directory query range.
   *
   * @var int
   */
  private $dir_query_range = 0;

  /**
   * Temporary filename.
   *
   * @var String
   */
  private $tmp_file_name = '';

  /**
   * Is a file being written too.
   *
   * @var Bool
   */
  private $is_write = FALSE;

  // As part of the inode protection mode returned by stat(), identifies the
  // file as a regular file, as opposed to a directory, symbolic link, or other
  // type of "file".
  // @see http://linux.die.net/man/2/stat
  const S_IFREG = 0100000;
  const S_IFDIR = 0040000;

  /**
   * Template for stat calls.
   *
   * All elements must be initialized.
   */
  protected $_stat = array(
    0 => 0, // Device number
    'dev' => 0,
    1 => 0, // Inode number
    'ino' => 0,
    // Inode protection mode. file_unmanaged_delete() requires is_file() to
    // return TRUE.
    2 => 0, //self::S_IFREG,
    'mode' => 0, //self::S_IFREG,
    3 => 0, // Number of links.
    'nlink' => 0,
    4 => 0, // Userid of owner.
    'uid' => 0,
    5 => 0, // Groupid of owner.
    'gid' => 0,
    6 => -1, // Device type, if inode device *
    'rdev' => -1,
    7 => 0, // Size in bytes.
    'size' => 0,
    8 => 0, // Time of last access (Unix timestamp).
    'atime' => 0,
    9 => 0, // Time of last modification (Unix timestamp).
    'mtime' => 0,
    10 => 0, // Time of last inode change (Unix timestamp).
    'ctime' => 0,
    11 => -1, // Blocksize of filesystem IO.
    'blksize' => -1,
    12 => -1, // Number of blocks allocated.
    'blocks' => -1,
  );

  /**
   * {@inheritdoc}
   */
  protected static $mapping = NULL;

  /**
   * {@inheritdoc}
   */
  function setUri($uri) {
    $this->uri = $uri;
  }

  /**
   * {@inheritdoc}
   */
  function getUri() {
    return $this->uri;
  }

  /**
   * {@inheritdoc}
   */
  public static function getMimeType($uri, $mapping = NULL) {
    // Load the default file map.
    if (!isset(self::$mapping)) {
      include_once DRUPAL_ROOT . '/includes/file.mimetypes.inc';
      self::$mapping = file_mimetype_mapping();
    }

    $extension = '';
    $file_parts = explode('.', basename($uri));
    // Remove the first part: a full filename should not match an extension.
    array_shift($file_parts);

    // Iterate over the file parts, trying to find a match.
    // For my.awesome.image.jpeg, we try:
    //   - jpeg
    //   - image.jpeg, and
    //   - awesome.image.jpeg
    while ($additional_part = array_pop($file_parts)) {
      $extension = strtolower($additional_part . ($extension ? '.' . $extension : ''));
      if (isset(self::$mapping['extensions'][$extension])) {
        return self::$mapping['mimetypes'][self::$mapping['extensions'][$extension]];
      }
    }

    return 'application/octet-stream';
  }

  /**
   * {@inheritdoc}
   */
  public function chmod($mode) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function realpath() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function stream_open($uri, $mode, $options, &$opened_path) {
    $allowed_modes = array('r', 'rb', 'w', 'wb');
    if (!in_array($mode, $allowed_modes)) {
      return FALSE;
    }
    $this->uri = $uri;
    // If the stream was opened for reading.
    if ($mode == 'r' || $mode == 'rb') {
      $path = $this->getLocalPath($uri);

      if ($options & STREAM_REPORT_ERRORS) {
        $this->handle = fopen($path, $mode);
      }
      else {
        $this->handle = @fopen($path, $mode);
      }
      if (empty($this->handle)) {
        watchdog('mw', 'The file %file failed to open in %mode mode.', array('%file' => $path, '%mode' => $mode), WATCHDOG_ERROR);
      }
    }// If the stream was opened for writing.
    elseif ($mode == 'w' || $mode == 'wb') {
      $this->is_write = TRUE;
      // Generate a temporary filename.
      $tmpfname = tempnam($this->getDirectoryPath() . '/tmp', 'mw');
      $this->tmp_file_name = $tmpfname;
      $this->handle = @fopen($tmpfname, $mode);
      if (empty($this->handle)) {
        watchdog('mw', 'The temp file %file failed to open in %mode mode.', array('%file' => $tmpfname, '%mode' => $mode), WATCHDOG_ERROR);
      }
    }

    return !empty($this->handle);
  }

  /**
   * {@inheritdoc}
   */
  public function stream_lock($operation) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function stream_read($count) {
      $read = fread($this->handle, $count);
      if ($read === FALSE) {
        watchdog('mw', 'fread of %file failed (stream_read)', array('%file' => $this->uri), WATCHDOG_ERROR);
      }
      return $read;
  }

  /**
   * {@inheritdoc}
   */
  public function stream_write($data) {
    if (!empty($this->handle)) {
      $write = fwrite($this->handle, $data);
      if ($write === FALSE) {
        watchdog('mw', 'fwrite of %file failed ', array('%file' => $this->uri), WATCHDOG_ERROR);
      }
      return $write;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function stream_eof() {
    return feof($this->handle);
  }

  /**
   * {@inheritdoc}
   */
  public function stream_seek($offset, $whence) {
    return fseek($this->handle, $offset, $whence);
  }

  /**
   * {@inheritdoc}
   */
  public function stream_flush() {
    return fflush($this->handle);
  }

  /**
   * {@inheritdoc}
   */
  public function stream_tell() {
    return ftell($this->handle);
  }

  /**
   * {@inheritdoc}
   */
  public function stream_stat() {
    return $this->_stat();
  }


  /**
   * {@inheritdoc}
   */
  public function stream_close() {
    global $user; // need this to get user id

    if ($this->is_write) {
      // Close the stream.
      fclose($this->handle);
      // Create a hash of the file and append the Media Warehouse file
      // extension.
      $file_hash = hash_file('sha512', $this->tmp_file_name) . '.mw';
      // Check to see if the file hash already exists.
      $file_exists = db_select('media_warehouse_hash', 'm')
        ->fields('m', array('hash'))
        ->condition('hash', $file_hash, '=')
        ->execute()
        ->fetchAssoc();

      // If the file doesn't already exist move the file from it's temporary
      // location the file system.
      if (empty($file_exists) || $file_exists == FALSE) {
        @copy($this->tmp_file_name, $this->getSystemPath($file_hash) . '/' . $file_hash);
      }
      // Delete the temporary file.
      unlink($this->tmp_file_name);
      $url_path = $this->getTarget($this->uri);
      $file_size = @filesize($this->getSystemPath($file_hash) . '/' . $file_hash);
      if ($file_size === FALSE || $file_size === 0) {
        watchdog('mw', '%file with a virtual path of %vpath has a filesize of %filesize.', array('%file' => $this->getSystemPath($file_hash) . '/' . $file_hash, '%vpath' => $url_path, '%filesize' => $file_size), WATCHDOG_ERROR);
      }
      else {
        // Remove the file from the deletion table if it's hash matches the
        // uploaded file's hash.
        $remove_pending_deletion = db_delete('media_warehouse_deletion')
          ->condition('hash', $file_hash, '=')
          ->execute();
        $key_exists = db_select('media_warehouse_hash', 'm')
          ->fields('m', array('hash'))
          ->condition('mwkey', $url_path, '=')
          ->execute()
          ->fetchAssoc();
        if (!empty($key_exists) && $key_exists['hash'] != $file_hash) {
          $this->unlink('mw://' . $url_path, 'update');
        }
        db_merge('media_warehouse_hash')
          ->key(array('mwkey' => $url_path))
          ->insertFields(array(
            'hash' => $file_hash,
            'mwkey' => $url_path,
          ))
          ->updatefields(array(
            'hash' => $file_hash,
          ))
          ->execute();
        // Successful write!
        return TRUE;
      }
      // If the filesize is 0, return FALSE.
      return FALSE;
    }
    // If we get here the handle has not been closed.
    return fclose($this->handle);
  }

  /**
   * {@inheritdoc}
   */
  public function unlink($uri, $op = NULL) {
    $url_path = $this->getTarget($uri);
    // Get the file hash by it's Media Warehouse key.
    $file = db_select('media_warehouse_hash', 'm')
      ->fields('m', array('hash'))
      ->condition('mwkey', $url_path, '=')
      ->execute()
      ->fetchAssoc();
    if (empty($file) || $file == FALSE) {
      return FALSE;
    }
    $hash_occurrences = db_select('media_warehouse_hash', 'h')
      ->fields('h')
      ->condition('hash', $file['hash'], '=')
      ->execute();
    // Count the number of times this file has been uploaded.
    $hash_count = $hash_occurrences->rowCount();
    // Remove entry form all 3 Media Warehouse Tables
    $remove_from_hash_table = db_delete('media_warehouse_hash')
      ->condition('mwkey', $url_path, '=')
      ->execute();
    if ($op != 'update') {
      $remove_from_files_table = db_delete('media_warehouse_files')
        ->condition('mwkey', $url_path, '=')
        ->execute();

      $remove_from_owner_table = db_delete('media_warehouse_file_owner')
        ->condition('mwkey', $url_path, '=')
        ->execute();
    }
    // If there is only one occurrence of the file, mark it for deletion.
    if ($hash_count <= 1) {
      global $user;

      $file_to_delete = db_insert('media_warehouse_deletion')
        ->fields(array(
          'hash' => $file['hash'],
          'time' => time(),
          'uid' => $user->uid,
        ))
        ->execute();
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function rename($from_uri, $to_uri) {
    $from_uri_path = $this->getTarget($from_uri);
    $to_uri_path = $this->getTarget($to_uri);

    if ($from_uri_path === FALSE || $to_uri_path === FALSE) {
      return FALSE;
    }

    $rename = db_update('media_warehouse_hash')
      ->fields(array('mwkey' => $to_uri))
      ->condition('mwkey', $from_uri, '=')
      ->execute();

    if (empty($rename) || $rename == FALSE) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function mkdir($uri, $mode, $options) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function rmdir($uri, $options) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function url_stat($uri, $flags) {
    $this->uri = $uri;
    return $this->_stat($this->getLocalPath());
  }

  /**
   * {@inheritdoc}
   */
  protected function getLocalPath($uri = NULL) {
    if (!isset($uri)) {
      $uri = $this->uri;
    }
    $v_path = $this->getTarget($uri);
    $virtual_path = db_select('media_warehouse_hash', 'm')
      ->fields('m', array('mwkey', 'hash'))
      ->condition('mwkey', $v_path, '=')
      ->execute()
      ->fetchAssoc();
    if (!empty($virtual_path)) {
      $path = $this->getSystemPath($virtual_path['hash']) . '/' . $virtual_path['hash'];
    }
    else {
      $path = $this->getDirectoryPath() . '/' . $this->getTarget($uri);
    }
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function dir_opendir($uri, $options) {
    $this->dir = $this->getTarget($uri);
    $this->_count_dir($this->dir);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function dir_readdir() {
    $query_obj = $this->dir_query->fetchObject();
    $path = $query_obj->mwkey;

    if (empty($path)) {
      if ($this->_count_dir($this->dir) != 0) {
        $query_obj = $this->dir_query->fetchObject();
        $path = $query_obj->mwkey;
      }
      else {
        return FALSE;
      }
    }

    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function dir_rewinddir() {
    $this->dir_query_range = 0;
    $this->_count_dir();
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function dir_closedir() {
    $this->dir = NULL;
    unset($this->dir_query);
    return TRUE;
  }

  protected function _count_dir($uri) {
    $start_range = $this->dir_query_range;
    $this->dir_query_range += 50;

    $this->dir_query = db_select('media_warehouse_hash', 'm')
      ->fields('m', array('mwkey', 'hash'))
      ->condition('mwkey', '%' . $uri, 'LIKE')
      ->range($start_range, $this->dir_query_range)
     ->execute();

    return $this->dir_query->rowCount();
  }
  /**
   * {@inheritdoc}
   */
  public function getDirectoryPath() {
    $dir_path = variable_get('media_warehouse_dir_path', NULL);
    if (empty($dir_path)) {
      watchdog('mw', 'Invalid default directory path for Media Warehouse.', array(), WATCHDOG_ERROR, NULL);
    }
    return $dir_path;
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalUrl() {
    return $GLOBALS['base_url'] . '/mw/' . $this->getTarget($this->uri);
  }

  /**
   * {@inheritdoc}
   */
  public function dirname($uri = NULL) {
    $scheme = file_uri_scheme($uri);
    $target  = $this->getTarget($uri);
    $dirname = dirname($target);

    if ($dirname == '.') {
      $dirname = '';
    }

    return $scheme . '://' . $dirname;
  }

  /**
   * {@inheritdoc}
   */
  protected function getTarget($uri = NULL) {
    if (!isset($uri)) {
      $uri = $this->uri;
    }
    $target = file_uri_target($uri);
    if ($target === FALSE) {
      $target = '';
    }
    return $target;
  }

  /**
   * Determines stat information for a file or directory.
   *
   * @param $uri
   *   A string containing the URI of the resource to check.
   *
   * @return bool | array
   *   Returns stat information for a file or directory or FALSE if none is
   *   available.
   */
  protected function _stat($uri = NULL) {
    if (!isset($uri)) {
      $uri = $this->uri;
    }

    $mw_stat = $this->_stat;
    if ($this->_is_dir($uri)) {
      $mw_stat['mode'] = self::S_IFDIR;
      $mw_stat['mode'] |= 0777;
      $mw_stat[2] = $mw_stat['mode'];
      return $mw_stat;
    }
    elseif (@file_exists($uri)) {
      $mw_stat['mode'] = self::S_IFREG;
      $mw_stat['mode'] |= 0666;
      $mw_stat[2] = $mw_stat['mode'];
      $mw_stat[7] = $mw_stat['size'] = filesize($uri);
      $mw_stat[8] = $mw_stat['atime'] = time();
      $mw_stat[9] = $mw_stat['mtime'] = time();
      $mw_stat[10] = $mw_stat['ctime'] = time();

      return $mw_stat;
    }
    return FALSE;
  }

  /**
   * Determine whether if the $uri is a directory.
   *
   * @param $uri
   *   A string containing the URI of the resource to check.
   *
   * @return bool
   *   Returns TRUE if the resource is a directory or FALSE if it is not.
   */
  protected function _is_dir($uri = NULL) {
    if ($uri == NULL) {
      $uri = $this->uri;
    }
    if ($uri != NULL && !preg_match('/\./', $uri)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Determine the system path for a given hash.
   *
   * @param string $file_hash
   *   Hash of the file being requested.
   *
   * @return bool
   *   Returns a system path to the provided file.
   */
  private function getSystemPath($file_hash) {
    // Get the base directory path.
    $path = $this->getDirectoryPath();
    $system_path = $path;
    // Create three directories based on the first 3 characters of the hash.
    // This prevents from having large index files caused by having all our
    // files in one directory.
    for($i=0; $i<=2; ++$i) {
      if (!empty($file_hash[$i])) {
        $dir_path = $system_path . '/' . $file_hash[$i];
        // Check if the directory was already created.
        if (!@file_exists($dir_path)) {
          // Create the new directory.
          drupal_mkdir($dir_path, 002770);
        }
        $system_path .= '/' . $file_hash[$i];
      }
    }
    // Return the full system path.
    return $system_path;
  }
}